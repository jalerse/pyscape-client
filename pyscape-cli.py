#! /usr/bin/env/ python3

import argparse
import codecs
import csv
import json
import sys

from pyscape import Pyscape
from pyscape.fields import FIELDS

from time import time
from time import sleep
import logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

from concurrent.futures import ThreadPoolExecutor

def batch_urls(urls, n):
    """n = size of batches. 10 is recommended."""
    for i in range(0, len(urls), n):
        yield urls[i:i+n]

def write_csv(outfile, data):
    "generate csv output"

    output = []
    headers = []
    keys = ["uu",
            "us",
            "upl",
            "ueid",
            "peid",
            "uipl",
            "pid",
            "pmrp",
            "ptrp",
            "upa",
            "pda",
            "ut",
            ]
        
    for k in keys:
        try:
            headers.append(FIELDS[k]['human'])
        except:
            headers.append(k)

    output.append(headers)

    for record in data:
        line = []
        try:
            for k in keys:
                if k in record:
                    line.append(record[k])
                else:
                    line.append('')
            output.append(line)
        except:
            output.append(['Error parsing line.'])

    writer = csv.writer(outfile, delimiter = ',',
                        quotechar = '"',
                        dialect = 'excel',
                        quoting = csv.QUOTE_ALL)
    writer.writerows(output)

def read_txt(textFile):
    fh = open(textFile, 'r')
    api_id = []
    api_key = []
    for line in fh.readlines():
        yh = line.strip().split('|')
        api_id.append(yh[0])
        api_key.append(yh[1])
    fh.close()
    return (api_id, api_key)

def get_url():
    args = parser.parse_args()
    # List URLs
    urls = []
    with open(args.src, 'r') as s:
        for line in s:
            urls.append(line.rstrip())
    return urls


def worker_API(worker_id, worker_key, urls):
    p = Pyscape(worker_id, worker_key)
    # Getting the datas (request)
    data = []
    for batch in batch_urls(urls, 10): # (ursl, n) : n urls at a time
        data.extend(p.batch_url_metrics(batch).json())
        print('%s URLs returned' % (len(data)))
        # sleep(1)
        sleep(10) # Because every request needs 10 secs break
    return data

parser = argparse.ArgumentParser(description = 'Interface with the Mozscape API to provide link metrics')
parser.add_argument('src', help = 'specify a text file with URLs')
parser.add_argument('dest', help = 'specify an output file')

def main():
    ## Begin
    #################################################################
    ts = time()
    urls = get_url()
    args = parser.parse_args()
    # Masukin Apiproxy ke list
    (api_id, api_key) = read_txt('D:/pyEndi/textFile/apiproxyTest1.txt')
    #################################################################


    # ## Single Processing
    # #################################################################
    # data = worker_API(api_id[0], api_key[0],urls[0:30]) # Single Worker
    # data = worker_API(api_id[1], api_key[1],urls[31:60]) # Single Worker
    # dataUrl = worker_API(api_id[0], api_key[0], urls[0:20])
    # print(dataUrl)
    # #################################################################


    ## Multi Processing
    #################################################################
    dataUrl = []
    urls_list = []
    # -- Define total domain per API has to request
    urls_to_request = int(len(urls)/len(api_id))
    # -- Generate urls_list
    for i in range (1, len(api_id)+1): #+1 karena dimulai dari 1, untuk memudahkan aja
        x = (i-1)*urls_to_request
        y = i*urls_to_request
        urls_list.append(urls[x:y])
    # print (urls_list)

    # -- Working POOL!
    with ThreadPoolExecutor() as executor:
        for API, API_output in zip(api_id, executor.map(worker_API, api_id, api_key, urls_list)):
            # dataUrl = API_output
            dataUrl = dataUrl + API_output
            # print("with %s comes : %s" % (API,API_output))

    # print(urls_list[0])
    # print(" ")
    # print(urls_list[1])    
    # for API in range(0, len(api_id)):
    #     print("%d " % (API))

    # for i in range (0, 1):
    #     # print(i)
    #     x = i*50
    #     y = (i+1)*50
    #     # print ('%d : %d' % (x,y))
    #     # print(urls[x:y])
    #     data = worker_API(api_id[i], api_key[i], urls[x:y])
    # dataUrl = worker_API(api_id[0], api_key[0], urls[0:10])
        # dataUrl.append(data[i])
        # print(urls[x:y])
    # print(urls[1:50])
    # print(urls[49:100])
    # print(urls[0:50])
    # print(len(urls[0:
    # :100]))
    # print(urls[0:50])
    # print(urls[50:100])
            # dataUrl[i] = executor.map(worker_API(api_id[i], api_key[i], urls[(50*(i-1))+1:50*i]))
    # print(urls[100])
    # print(urls[0])
    # print(len(urls))
    #################################################################


    ## End
    #################################################################
    # Generate .csv
    with codecs.open(args.dest, 'w', encoding='utf-8') as outfile:
        write_csv(outfile, dataUrl)
    logging.info('Took %s', time() - ts)
    #################################################################

if __name__ == '__main__':
    sys.exit(main())